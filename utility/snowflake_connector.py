import pandas as pd
from snowflake import connector as sf
import numpy as np
import datetime
import snowflake.sqlalchemy
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
from .batchfile import *

conn = None


class SnowflakeConnector:
    """Snowflake Connector"""

    def __init__(self):
        self.sf = None

    def connect(self, **kwargs):
        global conn
        data_warehouse = kwargs.get("data_warehouse")
        user = kwargs.get("user")
        warehouse = kwargs.get("warehouse")

        if all([data_warehouse, user, warehouse]):
            # validate credentials
            if data_warehouse != 'Snowflake':
                return {"error": "Invalide Data Warehouse"}
            if user != User:
                return {"error": "Invalide User"}
            if warehouse != Warehouse:
                return {"error": "Invalide Warehouse"}
        # check connection is alive and valid
        if conn:
            try:
                conn.execute("select 1 as is_alive;")
            except:
                print("in exception connection")
                conn = None

        if not conn:
            engine = create_engine(URL(
                account=Account,
                user=User,
                authenticator=Authenticator,
                Warehouse=Warehouse
            ))

            conn = engine.connect()
            self.use_db_warehouse(Warehouse, Database)
        return conn

    def close_connection(self):
        global conn
        try:
            conn.close()
            conn = None
        except:
            conn = None

    @staticmethod
    def use_db_warehouse(warehouse, database):
        conn.execute(f"USE WAREHOUSE {warehouse}")
        conn.execute(f"USE DATABASE {database}")
