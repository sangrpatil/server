import pandas as pd
import os
import numpy as np
import datetime
import pytz

from .batchfile import *
from .platform_connector import PlatformConnector

def source_missingkey(Schema,Table_Name):
    conn = PlatformConnector().connect()
    last_update_dt = pd.Series(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].AUDIT_COLNAME.tolist())
    if SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].LOAD_TYPE.str.contains('INCREMENT').all():
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
    else:
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
    df.columns = map(str.upper, df.columns)
    dup = df.copy()
    key_list= pd.DataFrame(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].KEY.str.split(',').tolist()).loc[0, :].values.tolist()
    dup['COMPOSITE_KEY'] = dup[key_list].apply(lambda row: '_'.join(row.values.astype(str)), axis=1)
    F_MISS_LOG = pd.DataFrame(columns =['FILE_ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','SRC_MSTR_ID','ERR_CNT','FILENAME','DATE'])
    missing = dup['COMPOSITE_KEY'].isnull().sum()
    if missing>0:
        AUD_FILE_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.FILE_ERR_LOG;", conn)
        AUD_FILE_ERR_LOG.columns = map(str.upper, AUD_FILE_ERR_LOG.columns)
        M_ERR_LOG_ID = range(len(AUD_FILE_ERR_LOG), len(AUD_FILE_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        M_ERR_MSTR_ID = '1'
        SRC_MSTR = (np.where(SRC_MSTR_DF.SRC_FILE_NAME ==Table_Name,SRC_MSTR_DF.SRC_MSTR_ID,0)).tolist()
        M_SRC_MSTR_ID = SRC_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(SRC_MSTR) if val != 0]]))]
        ERR_CNT = missing
        M_FILENAME = Table_Name
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        M_DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        F_MISS_LOG = F_MISS_LOG.append(pd.Series([M_ERR_LOG_ID,M_BTCH_ID,M_ERR_MSTR_ID,M_SRC_MSTR_ID,ERR_CNT,M_FILENAME,M_DATE], index=F_MISS_LOG.columns ), ignore_index=True)
        F_MISS_LOG.to_sql("FILE_ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return F_MISS_LOG



def tgt_missingkeytest(Schema,Table_Name):
     conn = PlatformConnector().connect()
     JOB_MSTR_DF=pd.read_sql_query(f'select * from {AUDIT_SCHEMA}.JOB_MSTR;', conn)
     JOB_MSTR_DF.columns = map(str.upper, JOB_MSTR_DF.columns)
     JOB_MSTR_DF['KEY'] = JOB_MSTR_DF['KEY'].astype(str).str.upper()
     JOB_MSTR_DF['SCHEMA'] = JOB_MSTR_DF['PROC_NM'].astype(str).str.upper().str.split().str[2]
     JOB_MSTR_DF = JOB_MSTR_DF[(JOB_MSTR_DF['TGT_NM']==Table_Name) & (JOB_MSTR_DF['SCHEMA']==Schema.upper())]
     if JOB_MSTR_DF['LOAD_TYPE'].str.contains('INCREMENT').all():
         last_update_dt = JOB_MSTR_DF.AUDIT_COLNAME.tolist()
         col_list = pd.read_sql_query(f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME='{Table_Name}' AND TABLE_SCHEMA='{Schema}';", conn).column_name.tolist()
         if CURRENT_INDICATOR in col_list:
             df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name}) AND {CURRENT_INDICATOR}='{CURRENT_INDICATOR_VAL}';", conn)
         else:
             df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
     else:
         df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
     df.columns = map(str.upper, df.columns)
     dup = df.copy()

     key_list= pd.DataFrame(JOB_MSTR_DF[(JOB_MSTR_DF['TGT_NM']==Table_Name)].KEY.str.split(',').tolist()).loc[0, :].values.tolist()
     dup['COMPOSITE_KEY'] = dup[key_list].apply(lambda row: '_'.join(row.values.astype(str)), axis=1)
     MISS_LOG = pd.DataFrame(columns = ['ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','JOB_MSTR_ID','ERR_CNT','TABLENAME','DATE'])
     missing = dup['COMPOSITE_KEY'].isnull().sum()
     if missing>0:
        AUD_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.ERR_LOG;", conn)
        AUD_ERR_LOG.columns = map(str.upper, AUD_ERR_LOG.columns) 
        ERR_LOG_ID = range(len(AUD_ERR_LOG), len(AUD_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        ERR_MSTR_ID = '1'

        JOB_MSTR = (np.where(JOB_MSTR_DF.TGT_NM ==Table_Name,JOB_MSTR_DF.JOB_MSTR_ID,0)).tolist()
        JOB_MSTR_ID = JOB_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(JOB_MSTR) if val != 0]]))] 
        ERR_CNT = missing
        TABLENAME = Table_Name
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        MISS_LOG = MISS_LOG.append(pd.Series([ERR_LOG_ID,BTCH_ID,ERR_MSTR_ID,JOB_MSTR_ID,ERR_CNT,TABLENAME,DATE], index=MISS_LOG.columns ), ignore_index=True)
        MISS_LOG.to_sql("ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return MISS_LOG


#Date format test for Source Files/Tables {Schema} {Table_Name}

def s_dateformattest(Schema,Table_Name,dateformat):
     conn = PlatformConnector().connect()
     F_ERR_LOG = pd.DataFrame(columns =['FILE_ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','SRC_MSTR_ID','ERR_CNT','FILENAME','DATE'])

     df = pd.DataFrame(SRC_MSTR_DF.loc[SRC_MSTR_DF.SRC_FILE_NAME==Table_Name,'DATE_FIELD'])
     df.columns = map(str.upper, df.columns)
     dup = df.copy()
     dup = df.assign(DATE_FIELD=df['DATE_FIELD'].str.split(',')).explode('DATE_FIELD')

     date_list = dup['DATE_FIELD'].to_list()
     
     last_update_dt = pd.Series(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].AUDIT_COLNAME.tolist())
     
     if SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].LOAD_TYPE.str.contains('INCREMENT').all():
        get_date_info= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
     else:
        get_date_info= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
     
     
     #get_date_info = pd.read_sql_query(f"SELECT * FROM {Schema}.{Table_Name};", conn)
     get_date_info.columns = map(str.upper, get_date_info.columns)
     
     failed_records =pd.DataFrame()
     dateformat = '%Y-%m-%d'
     if date_list[0] != '':
         for i in range(len(date_list)):
             var = date_list[i]
             dup = get_date_info.copy()
             dup = get_date_info.loc[get_date_info[var] > '0001-01-01']
             dup['Fail'] = pd.to_datetime(dup[var], format=dateformat, errors='coerce').isna()
             failed_records = failed_records.append(dup.query('Fail == True'),ignore_index=True)
   
     failed_records = failed_records.drop_duplicates(subset=failed_records.columns.difference(['Fail']))

     if failed_records.empty == False:
        AUD_FILE_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.FILE_ERR_LOG;", conn)
        AUD_FILE_ERR_LOG.columns = map(str.upper, AUD_FILE_ERR_LOG.columns) 
        FILE_ERR_LOG_ID = range(len(AUD_FILE_ERR_LOG), len(AUD_FILE_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        ERR_MSTR_ID = '2'
        SRC_MSTR = (np.where(SRC_MSTR_DF.SRC_FILE_NAME ==Table_Name,SRC_MSTR_DF.SRC_MSTR_ID,0)).tolist()
        SRC_MSTR_ID = SRC_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(SRC_MSTR) if val != 0]]))] 
        FILENAME = Table_Name
        ERR_CNT = len(failed_records)
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        F_ERR_LOG = F_ERR_LOG.append(pd.Series([FILE_ERR_LOG_ID,BTCH_ID,ERR_MSTR_ID,SRC_MSTR_ID,ERR_CNT,FILENAME,DATE], index=F_ERR_LOG.columns ), ignore_index=True)
        F_ERR_LOG.to_sql("FILE_ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return F_ERR_LOG




##Composite key test for Source Table
def source_dupkey(Schema,Table_Name):
     conn = PlatformConnector().connect()
     last_update_dt = pd.Series(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].AUDIT_COLNAME.tolist())
     if SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].LOAD_TYPE.str.contains('INCREMENT').all():
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
     else:
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
     df.columns = map(str.upper, df.columns)
     dup = df.copy()
     key_list= pd.DataFrame(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].KEY.str.split(',').tolist()).loc[0, :].values.tolist()
     dup['COMPOSITE_KEY'] = dup[key_list].apply(lambda row: '_'.join(row.values.astype(str)), axis=1)
     F_ERR_LOG = pd.DataFrame(columns =['FILE_ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','SRC_MSTR_ID','ERR_CNT','FILENAME','DATE'])
     dup_df = pd.DataFrame(columns = ['COMPOSITE_KEY','DUP_TEST'])
     dup_df['COMPOSITE_KEY'] = dup['COMPOSITE_KEY']
     dup_df['DUP_TEST'] = dup['COMPOSITE_KEY'].duplicated()
     dup_count = dup_df[dup_df['DUP_TEST']==True].shape[0]
     if dup_count>0:  
        AUD_FILE_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.FILE_ERR_LOG;", conn)
        AUD_FILE_ERR_LOG.columns = map(str.upper, AUD_FILE_ERR_LOG.columns)
        FILE_ERR_LOG_ID = range(len(AUD_FILE_ERR_LOG), len(AUD_FILE_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        ERR_MSTR_ID = '3'
        SRC_MSTR = (np.where(SRC_MSTR_DF.SRC_FILE_NAME ==Table_Name,SRC_MSTR_DF.SRC_MSTR_ID,0)).tolist()
        SRC_MSTR_ID = SRC_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(SRC_MSTR) if val != 0]]))] 
        ERR_CNT = dup_count
        FILENAME = Table_Name
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        F_ERR_LOG = F_ERR_LOG.append(pd.Series([FILE_ERR_LOG_ID,BTCH_ID,ERR_MSTR_ID,SRC_MSTR_ID,ERR_CNT,FILENAME,DATE], index=F_ERR_LOG.columns ), ignore_index=True)
        F_ERR_LOG.to_sql("FILE_ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return F_ERR_LOG

##Composite key test for Target Tables based on type of load
def tgt_dupkey(Schema,Table_Name):
     conn = PlatformConnector().connect()
     JOB_MSTR_DF=pd.read_sql_query(f'select * from {AUDIT_SCHEMA}.JOB_MSTR;', conn)
     JOB_MSTR_DF.columns = map(str.upper, JOB_MSTR_DF.columns)
     JOB_MSTR_DF['KEY'] = JOB_MSTR_DF['KEY'].astype(str).str.upper()
     JOB_MSTR_DF['SCHEMA'] = JOB_MSTR_DF['PROC_NM'].astype(str).str.upper().str.split().str[2]
     JOB_MSTR_DF = JOB_MSTR_DF[(JOB_MSTR_DF['TGT_NM']==Table_Name) & (JOB_MSTR_DF['SCHEMA']==Schema.upper())]
     if JOB_MSTR_DF['LOAD_TYPE'].str.contains('INCREMENT').all():
         last_update_dt = JOB_MSTR_DF.AUDIT_COLNAME.tolist()
         col_list = pd.read_sql_query(f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME='{Table_Name}' AND TABLE_SCHEMA='{Schema}';", conn).column_name.tolist()
         if CURRENT_INDICATOR in col_list:
             df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name}) AND {CURRENT_INDICATOR}='{CURRENT_INDICATOR_VAL}';", conn)
         else:
             df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
     else:
         df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
     df.columns = map(str.upper, df.columns)
     dup = df.copy()
     key_list= pd.DataFrame(JOB_MSTR_DF[(JOB_MSTR_DF['TGT_NM']==Table_Name)].KEY.str.split(',').tolist()).loc[0, :].values.tolist()
     dup['COMPOSITE_KEY'] = dup[key_list].apply(lambda row: '_'.join(row.values.astype(str)), axis=1)
     F_ERR_LOG = pd.DataFrame(columns = ['ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','JOB_MSTR_ID','ERR_CNT','TABLENAME','DATE'])
     dup_df = pd.DataFrame(columns = ['COMPOSITE_KEY','DUP_TEST'])
     dup_df['COMPOSITE_KEY'] = dup['COMPOSITE_KEY']
     dup_df['DUP_TEST'] = dup['COMPOSITE_KEY'].duplicated()
     dup_count = dup_df[dup_df['DUP_TEST']==True].shape[0]
     if dup_count>0:
        AUD_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.ERR_LOG;", conn)
        AUD_ERR_LOG.columns = map(str.upper, AUD_ERR_LOG.columns)    
        ERR_LOG_ID = range(len(AUD_ERR_LOG), len(AUD_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        ERR_MSTR_ID = '3'
        JOB_MSTR = (np.where(JOB_MSTR_DF.TGT_NM ==Table_Name,JOB_MSTR_DF.JOB_MSTR_ID,0)).tolist()
        JOB_MSTR_ID = JOB_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(JOB_MSTR) if val != 0]]))] 
        ERR_CNT = dup_count
        TABLENAME = Table_Name
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        F_ERR_LOG = F_ERR_LOG.append(pd.Series([ERR_LOG_ID,BTCH_ID,ERR_MSTR_ID,JOB_MSTR_ID,ERR_CNT,TABLENAME,DATE], index=F_ERR_LOG.columns ), ignore_index=True)
        F_ERR_LOG.to_sql("ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return F_ERR_LOG

#Special_characters_check for Source Files/Tables {Schema} {Table_Name}
     
def src_specialcharactercheck(Schema,Table_Name,SpecialCharacters):
     conn = PlatformConnector().connect()
     F_ERR_LOG = pd.DataFrame(columns =['FILE_ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','SRC_MSTR_ID','ERR_CNT','FILENAME','DATE'])

     last_update_dt = pd.Series(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].AUDIT_COLNAME.tolist())
     if SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].LOAD_TYPE.str.contains('INCREMENT').all():
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
     else:
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
     df.columns = map(str.upper, df.columns)
     df1 = pd.read_sql_query(f"SELECT DATA_TYPE,COLUMN_NAME FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME='{Table_Name}' AND TABLE_SCHEMA='{Schema}';", conn)
     df1.columns = map(str.upper, df1.columns)
     text_cols = df1[df1.DATA_TYPE == 'TEXT']
     text_cols = text_cols['COLUMN_NAME'].to_list()
 
     special_records =pd.DataFrame()
     var = SpecialCharacters
     for y in range(len(text_cols)):              
         col = text_cols[y]
         special_records = special_records.append(df.loc[df[col].str.contains(r'{}'.format(var), flags=8 , na=False)]).drop_duplicates()
         #special_records = special_records.append(df.loc[df[col].str.contains(r'[#$^%"<>?/~`!-]', flags=8 , na=False)]).drop_duplicates()
                
     if special_records.empty == False:
        AUD_FILE_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.FILE_ERR_LOG;", conn)
        AUD_FILE_ERR_LOG.columns = map(str.upper, AUD_FILE_ERR_LOG.columns)
        FILE_ERR_LOG_ID = range(len(AUD_FILE_ERR_LOG), len(AUD_FILE_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        ERR_MSTR_ID = '4'
        
        SRC_MSTR = (np.where(SRC_MSTR_DF.SRC_FILE_NAME ==Table_Name,SRC_MSTR_DF.SRC_MSTR_ID,0)).tolist()
        SRC_MSTR_ID = SRC_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(SRC_MSTR) if val != 0]]))] 
        ERR_CNT = len(special_records)
        FILENAME = Table_Name
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        F_ERR_LOG = F_ERR_LOG.append(pd.Series([FILE_ERR_LOG_ID,BTCH_ID,ERR_MSTR_ID,SRC_MSTR_ID,ERR_CNT,FILENAME,DATE], index=F_ERR_LOG.columns ), ignore_index=True)
        F_ERR_LOG.to_sql("FILE_ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return F_ERR_LOG


   



## Special Character check based on table load type
def tgt_specialcharactercheck(Schema,Table_Name,SpecialCharacters):
     conn = PlatformConnector().connect()
     JOB_MSTR_DF=pd.read_sql_query(f'select * from {AUDIT_SCHEMA}.JOB_MSTR;', conn)
     JOB_MSTR_DF.columns = map(str.upper, JOB_MSTR_DF.columns)
     JOB_MSTR_DF['KEY'] = JOB_MSTR_DF['KEY'].astype(str).str.upper()
     JOB_MSTR_DF['SCHEMA'] = JOB_MSTR_DF['PROC_NM'].astype(str).str.upper().str.split().str[2]
     JOB_MSTR_DF = JOB_MSTR_DF[(JOB_MSTR_DF['TGT_NM']==Table_Name) & (JOB_MSTR_DF['SCHEMA']==Schema.upper())]
     if JOB_MSTR_DF['LOAD_TYPE'].str.contains('INCREMENT').all():
         last_update_dt = JOB_MSTR_DF.AUDIT_COLNAME.tolist()
         col_list = pd.read_sql_query(f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME='{Table_Name}' AND TABLE_SCHEMA='{Schema}';", conn).column_name.tolist()
         if CURRENT_INDICATOR in col_list:
             df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name}) AND {CURRENT_INDICATOR}='{CURRENT_INDICATOR_VAL}';", conn)
         else:
             df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
     else:
         df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
     df.columns = map(str.upper, df.columns)

     df1 = pd.read_sql_query(f"SELECT DATA_TYPE,COLUMN_NAME FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME='{Table_Name}' AND TABLE_SCHEMA='{Schema}';", conn)
     df1.columns = map(str.upper, df1.columns)
     text_cols = df1[df1.DATA_TYPE == 'TEXT']
     text_cols = text_cols['COLUMN_NAME'].to_list()
     ERR_LOG = pd.DataFrame(columns = ['ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','JOB_MSTR_ID','ERR_CNT','TABLENAME','DATE'])
     special_records =pd.DataFrame()
     var = SpecialCharacters
     for y in range(len(text_cols)):              
         col = text_cols[y]
         special_records = special_records.append(df.loc[df[col].str.contains(r'{}'.format(var), flags=8 , na=False)]).drop_duplicates()
     
     if special_records.empty == False:
        AUD_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.ERR_LOG;", conn)
        AUD_ERR_LOG.columns = map(str.upper, AUD_ERR_LOG.columns) 
        ERR_LOG_ID = range(len(AUD_ERR_LOG), len(AUD_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        ERR_MSTR_ID = '4'

        JOB_MSTR = (np.where(JOB_MSTR_DF.TGT_NM ==Table_Name,JOB_MSTR_DF.JOB_MSTR_ID,0)).tolist()
        JOB_MSTR_ID = JOB_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(JOB_MSTR) if val != 0]]))] 
        ERR_CNT = len(special_records)
        TABLENAME = Table_Name
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        ERR_LOG = ERR_LOG.append(pd.Series([ERR_LOG_ID,BTCH_ID,ERR_MSTR_ID,JOB_MSTR_ID,ERR_CNT,TABLENAME,DATE], index=ERR_LOG.columns ), ignore_index=True)
        ERR_LOG.to_sql("ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return ERR_LOG



#True Duplicate Test for Source Tables with count
def source_trueduplicate(Schema,Table_Name):
    conn = PlatformConnector().connect()

    last_update_dt = pd.Series(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].AUDIT_COLNAME.tolist())
    if SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].LOAD_TYPE.str.contains('INCREMENT').all():
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
    else:
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
    df.columns = map(str.upper, df.columns)
    dup = df.copy()
    F_DUP_LOG = pd.DataFrame(columns =['FILE_ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','SRC_MSTR_ID','ERR_CNT','FILENAME','DATE'])
    duplicates = dup[dup.duplicated()]
    if duplicates.empty == False:
        AUD_FILE_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.FILE_ERR_LOG;", conn)
        AUD_FILE_ERR_LOG.columns = map(str.upper, AUD_FILE_ERR_LOG.columns)
        M_ERR_LOG_ID = range(len(AUD_FILE_ERR_LOG), len(AUD_FILE_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        M_BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        M_ERR_MSTR_ID = '5'
        SRC_MSTR = (np.where(SRC_MSTR_DF.SRC_FILE_NAME ==Table_Name,SRC_MSTR_DF.SRC_MSTR_ID,0)).tolist()
        M_SRC_MSTR_ID = SRC_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(SRC_MSTR) if val != 0]]))] 
        ERR_CNT = duplicates.shape[0]
        M_FILENAME = Table_Name
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        M_DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        F_DUP_LOG = F_DUP_LOG.append(pd.Series([M_ERR_LOG_ID,M_BTCH_ID,M_ERR_MSTR_ID,M_SRC_MSTR_ID,ERR_CNT,M_FILENAME,M_DATE], index=F_DUP_LOG.columns ), ignore_index=True)
        F_DUP_LOG.to_sql("FILE_ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return F_DUP_LOG



 #True Duplicate Test for Target Tables based on table load type  
def tgt_trueduplicate(Schema,Table_Name):
     conn = PlatformConnector().connect()
     JOB_MSTR_DF=pd.read_sql_query(f'select * from {AUDIT_SCHEMA}.JOB_MSTR;', conn)
     JOB_MSTR_DF.columns = map(str.upper, JOB_MSTR_DF.columns)
     JOB_MSTR_DF['KEY'] = JOB_MSTR_DF['KEY'].astype(str).str.upper()
     JOB_MSTR_DF['SCHEMA'] = JOB_MSTR_DF['PROC_NM'].astype(str).str.upper().str.split().str[2]
     JOB_MSTR_DF = JOB_MSTR_DF[(JOB_MSTR_DF['TGT_NM']==Table_Name) & (JOB_MSTR_DF['SCHEMA']==Schema.upper())]
     if JOB_MSTR_DF['LOAD_TYPE'].str.contains('INCREMENT').all():
         last_update_dt = JOB_MSTR_DF.AUDIT_COLNAME.tolist()
         col_list = pd.read_sql_query(f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME='{Table_Name}' AND TABLE_SCHEMA='{Schema}';", conn).column_name.tolist()
         if CURRENT_INDICATOR in col_list:
             df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name}) AND {CURRENT_INDICATOR}='{CURRENT_INDICATOR_VAL}';", conn)
         else:
             df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
     else:
         df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
     df.columns = map(str.upper, df.columns)
     dup = df.copy()
     F_DUP_LOG = pd.DataFrame(columns = ['ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','JOB_MSTR_ID','ERR_CNT','TABLENAME','DATE'])
     duplicates = dup[dup.duplicated()]
     if duplicates.empty == False:
        AUD_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.ERR_LOG;", conn)
        AUD_ERR_LOG.columns = map(str.upper, AUD_ERR_LOG.columns) 
        ERR_LOG_ID = range(len(AUD_ERR_LOG), len(AUD_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        ERR_MSTR_ID = '5'

        JOB_MSTR = (np.where(JOB_MSTR_DF.TGT_NM ==Table_Name,JOB_MSTR_DF.JOB_MSTR_ID,0)).tolist()
        JOB_MSTR_ID = JOB_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(JOB_MSTR) if val != 0]]))] 
        ERR_CNT = duplicates.shape[0]
        TABLENAME = Table_Name
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        F_DUP_LOG = F_DUP_LOG.append(pd.Series([ERR_LOG_ID,BTCH_ID,ERR_MSTR_ID,JOB_MSTR_ID,ERR_CNT,TABLENAME,DATE], index=F_DUP_LOG.columns ), ignore_index=True)
        F_DUP_LOG.to_sql("ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return F_DUP_LOG



def source_negativenumeric(Schema,Table_Name):
    conn = PlatformConnector().connect()
    last_update_dt = pd.Series(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].AUDIT_COLNAME.tolist())
    if SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].LOAD_TYPE.str.contains('INCREMENT').all():
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
    else:
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
    df.columns = map(str.upper, df.columns)
    df_columns_list = df.columns.tolist()
    dup = df.copy()
    metric_list = pd.DataFrame(SRC_MSTR_DF[['FIRST_METRIC_NM','SECOND_METRIC_NM','THIRD_METRIC_NM']].values.tolist()).loc[0, :].values.tolist()
    common_items = list(set(df_columns_list).intersection(set(metric_list)))
    F_RAN_LOG = pd.DataFrame(columns =['FILE_ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','SRC_MSTR_ID','ERR_CNT','FILENAME','DATE'])
    if len(common_items)>0:
        for column_name in common_items:
            dup[column_name] = dup[column_name].astype(float)
            neg_value = dup[column_name].lt(0).tolist().count(True)
            if neg_value >0:
                AUD_FILE_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.FILE_ERR_LOG;", conn)
                AUD_FILE_ERR_LOG.columns = map(str.upper, AUD_FILE_ERR_LOG.columns)
                M_ERR_LOG_ID = range(len(AUD_FILE_ERR_LOG), len(AUD_FILE_ERR_LOG) + 1).stop
                BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
                BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
                M_BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
                M_ERR_MSTR_ID = '6'
                SRC_MSTR = (np.where(SRC_MSTR_DF.SRC_FILE_NAME ==Table_Name,SRC_MSTR_DF.SRC_MSTR_ID,0)).tolist()
                M_SRC_MSTR_ID = SRC_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(SRC_MSTR) if val != 0]]))] 
                ERR_CNT = neg_value 
                M_FILENAME = Table_Name
                now = datetime.datetime.now()
                tz = pytz.timezone('Asia/Kolkata')
                ist = now.astimezone(tz)
                M_DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
                F_RAN_LOG = F_RAN_LOG.append(pd.Series([M_ERR_LOG_ID,M_BTCH_ID,M_ERR_MSTR_ID,M_SRC_MSTR_ID,ERR_CNT,M_FILENAME,M_DATE], index=F_RAN_LOG.columns ), ignore_index=True)
                F_RAN_LOG.to_sql("FILE_ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
                return F_RAN_LOG




#SCD_check for Target Files/Tables {Schema} {Table_Name}    
def tgt_scdcheck(Schema,Table_Name):
     conn = PlatformConnector().connect()
     ERR_LOG = pd.DataFrame(columns = ['ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','JOB_MSTR_ID','ERR_CNT','TABLENAME','DATE'])
     JOB_MSTR_DF=pd.read_sql_query(f'select * from {AUDIT_SCHEMA}.JOB_MSTR;', conn)
     JOB_MSTR_DF.columns = map(str.upper, JOB_MSTR_DF.columns)
     JOB_MSTR_DF['KEY'] = JOB_MSTR_DF['KEY'].astype(str).str.upper()
     JOB_MSTR_DF['SCHEMA'] = JOB_MSTR_DF['PROC_NM'].astype(str).str.upper().str.split().str[2]
     JOB_MSTR_DF = JOB_MSTR_DF[(JOB_MSTR_DF['TGT_NM']==Table_Name) & (JOB_MSTR_DF['SCHEMA']==Schema.upper())]
     if JOB_MSTR_DF['LOAD_TYPE'].str.contains('INCREMENT').all():
         last_update_dt = JOB_MSTR_DF.AUDIT_COLNAME.tolist()
         df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
     else:
         df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
     df.columns = map(str.upper, df.columns)
     key_list= pd.DataFrame(JOB_MSTR_DF[(JOB_MSTR_DF['TGT_NM']==Table_Name)].KEY.str.split(',').tolist()).loc[0, :].values.tolist()

     df['COMPOSITE_KEY'] = df[key_list].apply(lambda row: '_'.join(row.values.astype(str)), axis=1)
     col_list = pd.read_sql_query(f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME='{Table_Name}' AND TABLE_SCHEMA='{Schema}';", conn).column_name.tolist()
     if CURRENT_INDICATOR in col_list:
         ind_unq_key = df.loc[df[CURRENT_INDICATOR] == CURRENT_INDICATOR_VAL, 'COMPOSITE_KEY'].tolist()
         unq_key = df['COMPOSITE_KEY'].unique().tolist()
         ind_missing_key = list(set(unq_key) - set(ind_unq_key))
     else:
        ind_missing_key=[]

     if len(ind_missing_key) > 0:
        AUD_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.ERR_LOG;", conn)
        AUD_ERR_LOG.columns = map(str.upper, AUD_ERR_LOG.columns) 
        ERR_LOG_ID = range(len(AUD_ERR_LOG), len(AUD_ERR_LOG) + 1).stop
        BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
        BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
        BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
        ERR_MSTR_ID = '7'
        JOB_MSTR = (np.where(JOB_MSTR_DF.TGT_NM ==Table_Name,JOB_MSTR_DF.JOB_MSTR_ID,0)).tolist()
        JOB_MSTR_ID = JOB_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(JOB_MSTR) if val != 0]]))] 
        TABLENAME = Table_Name
        ERR_CNT = len(ind_missing_key)
        now = datetime.datetime.now()
        tz = pytz.timezone('Asia/Kolkata')
        ist = now.astimezone(tz)
        DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
        ERR_LOG = ERR_LOG.append(pd.Series([ERR_LOG_ID,BTCH_ID,ERR_MSTR_ID,JOB_MSTR_ID,ERR_CNT,TABLENAME,DATE], index=ERR_LOG.columns ), ignore_index=True)
        ERR_LOG.to_sql("ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
        return ERR_LOG


def source_numerictype(Schema,Table_Name):
    conn = PlatformConnector().connect()
    last_update_dt = pd.Series(SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].AUDIT_COLNAME.tolist())
    if SRC_MSTR_DF[(SRC_MSTR_DF['SRC_FILE_NAME']==Table_Name)].LOAD_TYPE.str.contains('INCREMENT').all():
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name} where CAST({last_update_dt[0]} AS DATE) in (select MAX(CAST({last_update_dt[0]} AS DATE)) from {Schema}.{Table_Name});", conn)
    else:
        df= pd.read_sql_query(f"select * from {Schema}.{Table_Name}", conn)
    df.columns = map(str.upper, df.columns)
    df_columns_list = df.columns.tolist()
    dup = df.copy()
    metric_list = pd.DataFrame(SRC_MSTR_DF[['FIRST_METRIC_NM','SECOND_METRIC_NM','THIRD_METRIC_NM']].values.tolist()).loc[0, :].values.tolist()
    common_items = list(set(df_columns_list).intersection(set(metric_list)))
    F_RAN_LOG = pd.DataFrame(columns =['FILE_ERR_LOG_ID','BTCH_ID','ERR_MSTR_ID','SRC_MSTR_ID','ERR_CNT','FILENAME','DATE'])
    if len(common_items)>0:
        string_err = []
        for column_name in common_items:
            for index, row in dup.iterrows():
                try:
                    float(row[column_name])
                except:
                    string_err.append(index)
            if len(set(string_err)) >0:
                AUD_FILE_ERR_LOG = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.FILE_ERR_LOG;", conn)
                AUD_FILE_ERR_LOG.columns = map(str.upper, AUD_FILE_ERR_LOG.columns)
                M_ERR_LOG_ID = range(len(AUD_FILE_ERR_LOG), len(AUD_FILE_ERR_LOG) + 1).stop
                BTCH_STAT = pd.read_sql_query(f"select * from {AUDIT_SCHEMA}.BTCH_STAT;", conn)
                BTCH_STAT.columns = map(str.upper, BTCH_STAT.columns)
                M_BTCH_ID = max(pd.to_numeric(BTCH_STAT.BTCH_ID))
                ERR_MSTR_ID = '8'
                SRC_MSTR = (np.where(SRC_MSTR_DF.SRC_FILE_NAME ==Table_Name,SRC_MSTR_DF.SRC_MSTR_ID,0)).tolist()
                M_SRC_MSTR_ID = SRC_MSTR[int(' '.join([str(elem) for elem in [idx for idx, val in enumerate(SRC_MSTR) if val != 0]]))] 
                ERR_CNT = len(set(string_err)) 
                M_FILENAME = Table_Name
                now = datetime.datetime.now()
                tz = pytz.timezone('Asia/Kolkata')
                ist = now.astimezone(tz)
                M_DATE = pd.to_datetime(ist).strftime("%m/%d/%Y %H:%M:%S")
                F_RAN_LOG = F_RAN_LOG.append(pd.Series([M_ERR_LOG_ID,M_BTCH_ID,M_ERR_MSTR_ID,M_SRC_MSTR_ID,ERR_CNT,M_FILENAME,M_DATE], index=F_RAN_LOG.columns ), ignore_index=True)
                F_RAN_LOG.to_sql("FILE_ERR_LOG", con=conn, schema=AUDIT_SCHEMA,if_exists='append', index=False)
                return F_RAN_LOG