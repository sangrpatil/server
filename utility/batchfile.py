Data_Platform = "snowflake"
Warehouse = 'CIRDE_BI_WH'
Database = 'CIRDE_PROD_DB'

AUDIT_SCHEMA = 'AUDIT'  ##schema where the Master tables are loaded
Source_Schema = 'SOURCE_RAW'
STAGE = 'PRESTAGE'
TARGET = ['STAGE','HYBRIS_BI']  ##add as a list

Account = 'cat.us-east-1'
User = 'boggala_swetha@cat.com'
Authenticator='externalbrowser'

Dateformat='YYYY-MM-DD'
SpecialCharacters='[$^%<>*?`!]'

TGT_Effective_Dt = 'EFFECTIVE_DATE'
CURRENT_INDICATOR = 'CURRENT_INDICATOR'
CURRENT_INDICATOR_VAL = 'Y'