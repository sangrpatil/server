from .batchfile import *
import importlib
import sys
from .snowflake_connector import SnowflakeConnector

data_platform = ""

class PlatformConnector:

	def __init__(self):
		self.data_platform = Data_Platform

	def connect(self, **kwargs):
		# module/file name must be in small case: data_platform + _connector e.g. snowflake_connector
		# class name should be like: data_platform + 'Connector - e.g. class SnowflakeConnector:
		global data_platform
		data_platform = kwargs.get('data_warehouse') or self.data_platform
		print("DataPlatform ####", data_platform)
		try:
			module = __import__('utility.' + data_platform + '_connector', fromlist=[data_platform])
			class_name = data_platform.title() + "Connector"
			platform_class = getattr(module, class_name)
			return platform_class().connect(**kwargs)
		except Exception as e:
			print("Connector not found", str(e))
			return None