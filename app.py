from flask import Flask
from views.error_report import error_blueprint
from views.filters import filters_blueprint
from views.validation import validation_blueprint
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.register_blueprint(error_blueprint)
app.register_blueprint(filters_blueprint)
app.register_blueprint(validation_blueprint)

if __name__ == '__main__':
    app.run(port=8000, debug=True)
