import json
from flask import Blueprint, request, jsonify
from utility.platform_connector import PlatformConnector
import pandas as pd
from datetime import datetime, date, timedelta
from utility.batchfile import *

error_blueprint = Blueprint('error', __name__, url_prefix='/error')


@error_blueprint.route('/count-by-type', methods=['GET'])
def type_count():
    try:
        conn = PlatformConnector().connect()
        # current month dates
        dt = datetime.now()
        end_date = dt.strftime("%Y-%m-%d")
        start_date = str(dt.year) + "-" + str(dt.month) + "-01"
        query = f"""
            select m.ERR_DESC,all_err.err_cnt as count, all_err.date as validation_date,btch.btch_dt as Batch_date
            from (
            with err_cnt_type as (
            
            select err_log.err_mstr_id,cast(err_log.err_cnt as int) as err_cnt,err_log.tablename,err_log.date,btch_id,
            row_number() over (partition by err_mstr_id,tablename,cast(date as date) order by  date desc) as row_num
            from 
            (select btch_id,ERR_MSTR_ID, ERR_CNT,filename as tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
            union
            select btch_id,ERR_MSTR_ID, ERR_CNT,tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG") err_log

            WHERE cast(date as date) between '{start_date}' and '{end_date}'
            
            ) select err_mstr_id,sum(err_cnt) as err_cnt ,cast(date as date) as date,max(btch_id) as btch_id,row_number() over (partition by err_mstr_id,cast(date as date) order by err_mstr_id ) as rn from err_cnt_type where row_num='1' 
            group by err_mstr_id,cast(date as date) order by err_mstr_id desc 
            ) all_err
            JOIN "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" m on ALL_ERR.err_mstr_id=M.ERR_CODE
            join "{Database}"."{AUDIT_SCHEMA}"."btch_stat" btch on all_err.btch_id =btch.btch_id

            UNION

            select err_desc,'0' as count,current_date() as validation_date,current_date() as Batch_date from "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" where ERR_DESC not in (

            select m.ERR_DESC
            from (
            with err_cnt_type as (

            select err_log.err_mstr_id,cast(err_log.err_cnt as int) as err_cnt,err_log.tablename,err_log.date,btch_id,
            row_number() over (partition by err_mstr_id,tablename,cast(date as date) order by  date desc) as row_num
            from 
            (select btch_id,ERR_MSTR_ID, ERR_CNT,filename as tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
            union
            select btch_id,ERR_MSTR_ID, ERR_CNT,tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG") err_log

            WHERE cast(date as date) between '{start_date}' and '{end_date}'

            ) select err_mstr_id,sum(err_cnt) as err_cnt ,cast(date as date) as date,max(btch_id) as btch_id,row_number() over (partition by err_mstr_id,cast(date as date) order by err_mstr_id ) as rn from err_cnt_type where row_num='1' 
            group by err_mstr_id,cast(date as date) order by err_mstr_id desc 
            ) all_err
            JOIN "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" m on ALL_ERR.err_mstr_id=M.ERR_CODE
            join "{Database}"."{AUDIT_SCHEMA}"."btch_stat" btch on all_err.btch_id =btch.btch_id

            );
        """
        data = pd.read_sql_query(query, conn)
        json_data = json.loads(data.to_json(orient="records", date_format="iso"))
        json_data = sorted(json_data, key = lambda i: i['validation_date'])
        result = dict()

        for d in json_data:
            error_desc = d.pop("err_desc")
            d["date"] = d.get("validation_date").split("T")[0]
            d.pop("validation_date")
            d.pop("batch_date")
            if error_desc in result:
                result.get(error_desc).append(d)
            else:
                result[error_desc] = [d]
        return jsonify(data=result)
    except Exception as e:
        print("count_by_type", str(e))
        return jsonify(error="Unable to fetch the data.")


@error_blueprint.route('/cumulative-count', methods=['GET'])
def cumulative_count(rolling_period=None):
    try:
        if not rolling_period:
            rolling_period = int(request.args.get("rolling_period", 3))
            rolling_period = 3 if rolling_period not in [3, 6] else rolling_period
        conn = PlatformConnector().connect()
        query = f"""
            select m.ERR_DESC,all_err.err_cnt as count, all_err.date as validation_date,btch.btch_dt as Batch_date
            from (
            with err_cnt_type as (
            
            select err_log.err_mstr_id,cast(err_log.err_cnt as int) as err_cnt,err_log.tablename,err_log.date,btch_id,
            row_number() over (partition by err_mstr_id,tablename,cast(date as date) order by  date desc) as row_num
            from 
            (select btch_id,ERR_MSTR_ID, ERR_CNT,filename as tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
            union
            select btch_id,ERR_MSTR_ID, ERR_CNT,tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG") err_log

            WHERE date >= DATEADD(MONTH, -{rolling_period}, GETDATE()) 
            
            ) select err_mstr_id,sum(err_cnt) as err_cnt ,cast(date as date) as date,max(btch_id) as btch_id,row_number() over (partition by err_mstr_id,cast(date as date) order by err_mstr_id ) as rn from err_cnt_type where row_num='1' 
            group by err_mstr_id,cast(date as date) order by err_mstr_id desc 
            ) all_err
            JOIN "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" m on ALL_ERR.err_mstr_id=M.ERR_CODE
            join "{Database}"."{AUDIT_SCHEMA}"."btch_stat" btch on all_err.btch_id =btch.btch_id

            UNION

            select err_desc,'0' as count,current_date() as validation_date,current_date() as Batch_date from "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" where ERR_DESC not in (

            select m.ERR_DESC
            from (
            with err_cnt_type as (

            select err_log.err_mstr_id,cast(err_log.err_cnt as int) as err_cnt,err_log.tablename,err_log.date,btch_id,
            row_number() over (partition by err_mstr_id,tablename,cast(date as date) order by  date desc) as row_num
            from 
            (select btch_id,ERR_MSTR_ID, ERR_CNT,filename as tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
            union
            select btch_id,ERR_MSTR_ID, ERR_CNT,tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG") err_log

            WHERE date >= DATEADD(MONTH, -{rolling_period}, GETDATE()) 

            ) select err_mstr_id,sum(err_cnt) as err_cnt ,cast(date as date) as date,max(btch_id) as btch_id,row_number() over (partition by err_mstr_id,cast(date as date) order by err_mstr_id ) as rn from err_cnt_type where row_num='1' 
            group by err_mstr_id,cast(date as date) order by err_mstr_id desc 
            ) all_err
            JOIN "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" m on ALL_ERR.err_mstr_id=M.ERR_CODE
            join "{Database}"."{AUDIT_SCHEMA}"."btch_stat" btch on all_err.btch_id =btch.btch_id

            );
        """
        data = pd.read_sql_query(query, conn)
        json_data = json.loads(data.to_json(orient="records", date_format="iso"))
        result = dict()
        for d in json_data:
            error_desc = d.pop("err_desc")
            if error_desc in result:
                updated_count = result.get(error_desc) + int(d.get("count"))
                result[error_desc] = updated_count
            else:
                result[error_desc] = int(d.get("count"))
        data_ = [{"name": k, "count": str(v)} for k, v in result.items()]
        return jsonify(data=data_)
    except Exception as e:
        print("############", str(e))
        return jsonify(error="Unable to fetch the data.")


@error_blueprint.route('current-vs-prior', methods=['GET'])
def current_vs_prior():
    try:
        # current and prioir year-month 
        month = request.args.get("month")
        year = request.args.get("year")

        current_date = str(year) + str(month)

        current_dt = str(year) + "-" + str(month) + "-"  + '01'
        current_dt = datetime.strptime(current_dt, '%Y-%m-%d').date()

        prior_dt = current_dt - timedelta(days=1)

        prior_date = str(prior_dt.year) + str(prior_dt.month)
        
        conn = PlatformConnector().connect()
        query = f"""select err_desc as type, nvl(sum(current_month_cnt),0) as error_count, cast(abs(prior) as int) as prior,indicator from (

            select m.err_desc as err_desc ,

            sum(case when monthyear= {current_date} then err_instance end) as current_month_cnt,
            sum(case when monthyear= {prior_date} then err_instance end) as last_month_cnt,

            case when current_month_cnt is null then -100
                when last_month_cnt is null then 100
                else
            100* (current_month_cnt - last_month_cnt) / last_month_cnt 
            end as prior,

            case when sign(prior) = 1 then 'up' 
                when sign(prior) = -1 then 'down'
                else 'neutral'
            end as Indicator


            from (
            
            with curr_prior as (
            select err_log.err_mstr_id,date_part(year,cast(err_log.DATE as DATE))||date_part(month,cast(err_log.date as date)) as monthyear,
            row_number() over (partition by err_mstr_id,cast(err_log.date as date) order by  err_log.date desc) as row_num
            
            from 
            (select ERR_MSTR_ID, filename as tablename,DATE  from "CIRDE_PROD_DB"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
            union
            select ERR_MSTR_ID, tablename,DATE  from "CIRDE_PROD_DB"."{AUDIT_SCHEMA}"."ERR_LOG") err_log

            ) select err_mstr_id,count(err_mstr_id) as err_instance ,monthyear as monthyear --, row_number() over (partition by err_mstr_id order by err_instance desc) as rn 
            from curr_prior 
            where row_num='1' 
            group by err_mstr_id,monthyear order by err_mstr_id, err_instance desc

            ) err
            JOIN "CIRDE_PROD_DB"."{AUDIT_SCHEMA}"."ERR_MSTR" m on err.ERR_MSTR_ID=M.ERR_CODE

            group by 1
            ) group by 1,3,4
        ;"""
        data = pd.read_sql_query(query, conn)
        json_data = json.loads(data.to_json(orient="records", date_format="iso"))
        return jsonify(data=json_data)
    except Exception as e:
        print("###ERROR", str(e))
        return jsonify(error="Unable to fetch the data.")


@error_blueprint.route('sync-indicator', methods=['GET'])
def sync_indicator():
    try:
        conn = PlatformConnector().connect()
        query  = '''
        SELECT
            CAST(DATE AS DATE) AS Batch_Date,  FILENAME AS Table_Name, METRIC_NM as Metric_Name,
            SRC_VAL as SRC_Value, STG_VAL as STG_Value,TGT_VAL as TGT_Value
        FROM
            "{Database}"."{AUDIT_SCHEMA}"."BAL_RECON" where status = 'I';
        '''
        data = pd.read_sql_query(query, conn)
        json_data = json.loads(data.to_json(orient="records", date_format="iso"))
        [ d.update({"batch_date": d["batch_date"].split("T")[0]}) for d in json_data ]
        return jsonify(data=json_data)
    except Exception as e:
        return jsonify(error="Unable to fetch the data.")


@error_blueprint.route('error-details', methods=['GET'])
def error_details():
    try:
        rolling_period = request.args.get("rolling_period")
        date_query = ""
        if rolling_period:
            rolling_period = 3 if rolling_period not in [3, 6] else int(rolling_period)

            date_query = f"and cast(date as date) >= DATEADD(MONTH, -{rolling_period}, GETDATE())"
        else:
            dt = datetime.now()
            end_date = dt.strftime("%Y-%m-%d")
            start_date = str(dt.year) + "-" + str(dt.month) + "-01"
            date_query = f"and cast(date as date) between '{start_date}' and '{end_date}'"

        query = f"""select btch.btch_dt as Batch_date,table_name,all_err.err_cnt as Error_Count,
            m.ERR_DESC as Error_desc,m.ERR_SVRTY as error_severity ,SOURCE_FILE_FLAG, all_err.date as validation_date
            from (
            with smmry_rpt as (
            
            select err_log.err_mstr_id,cast(err_log.err_cnt as int) as err_cnt,err_log.tablename,err_log.date,btch_id,SOURCE_FILE_FLAG,
            row_number() over (partition by err_mstr_id,tablename,cast(date as date) order by  date desc) as row_num
            from 
            (select btch_id,ERR_MSTR_ID, ERR_CNT,filename as tablename,DATE ,'Y' AS SOURCE_FILE_FLAG from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
            union
            select btch_id,ERR_MSTR_ID, ERR_CNT,tablename,DATE ,'N' AS SOURCE_FILE_FLAG from "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG") err_log

            where 1 = 1 {date_query}

            ) select err_mstr_id,max(err_cnt) as err_cnt ,max(tablename) as table_name ,max(SOURCE_FILE_FLAG) as SOURCE_FILE_FLAG ,cast(date as date) as date,
            max(btch_id) as btch_id,row_number() over (partition by err_mstr_id,tablename,cast(date as date) order by err_mstr_id ) as rn from smmry_rpt where row_num='1' 
            group by err_mstr_id,cast(date as date),tablename order by err_mstr_id desc 
            ) all_err
            JOIN "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" m on ALL_ERR.err_mstr_id=M.ERR_CODE
            join "{Database}"."{AUDIT_SCHEMA}"."btch_stat" btch on all_err.btch_id =btch.btch_id 
            order by batch_date desc;
        """
        conn = PlatformConnector().connect()
        data = pd.read_sql_query(query, conn)
        json_data = json.loads(data.to_json(orient="records", date_format="iso"))
        [ d.update({
            "batch_date": d["batch_date"].split("T")[0],
            "validation_date": d["validation_date"].split("T")[0],
            })
            for d in json_data
        ]
        return jsonify(data=json_data)
    except Exception as e:
        print("###########################", str(e))
        return jsonify(error="Unable to fetch the data.")


@error_blueprint.route('max-error-tables', methods=['GET'])
def max_error_tables():
    try:
        conn = PlatformConnector().connect()
        month = request.args.get("month")
        year = request.args.get("year")
        err_desc = request.args.get("error_desc")

        start_date = str(year) + "-" + str(month) + "-"  + '01'
        end_date = date(int(year), int(month) + 1, 1) - timedelta(days=1)
        end_date = end_date.strftime("%Y-%m-%d")
        query = f"""select err_desc as err_desc,tablename,err_instance as err_cnt from (
            select m.err_desc as err_desc,tablename,err_instance  from (
            with top10 as (
            
            select err_log.err_mstr_id,cast(err_log.err_cnt as int) as err_cnt,err_log.tablename,err_log.date,
            row_number() over (partition by err_mstr_id,tablename,cast(date as date) order by  date desc) as row_num
            from 
            (select ERR_MSTR_ID, ERR_CNT,filename as tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
            union
            select ERR_MSTR_ID, ERR_CNT,tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG") err_log

            where  date_part(month,cast(DATE as DATE)) = '{month}' AND date_part(year,cast(DATE as DATE)) ='{year}'
            
            ) select err_mstr_id,tablename,count(tablename) as err_instance ,max(date), row_number() over (partition by err_mstr_id order by err_instance desc) as rn from top10 where row_num='1' 
            group by err_mstr_id,tablename order by err_mstr_id, err_instance desc
            ) a 
            
            JOIN "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" m on a.ERR_MSTR_ID=M.ERR_CODE
            where rn <=10 

            
            union
            
            select err_desc , 'None' as tablename ,'0' as err_instance   from "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" where err_desc not in (
            select distinct m.err_desc as err_desc  from (
            with top10 as (
            
            select err_log.err_mstr_id,cast(err_log.err_cnt as int) as err_cnt,err_log.tablename,err_log.date,
            row_number() over (partition by err_mstr_id,tablename,cast(date as date) order by  date desc) as row_num
            from 

            (select ERR_MSTR_ID, ERR_CNT,filename as tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
            union
            select ERR_MSTR_ID, ERR_CNT,tablename,DATE  from "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG") err_log

            where  date_part(month,cast(DATE as DATE)) = '{month}' AND date_part(year,cast(DATE as DATE)) ='{year}'
            
            ) select err_mstr_id,tablename,count(tablename) as err_instance ,max(date), row_number() over (partition by err_mstr_id order by err_instance desc) as rn from top10 where row_num='1' 
            group by err_mstr_id,tablename order by err_mstr_id, err_instance desc
            ) a 
            
            JOIN "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" m on a.ERR_MSTR_ID=M.ERR_CODE
            where rn <=10

                ) 
            ) 
            where  tablename not in ('None') and  err_desc ='{err_desc}'
            
        ;"""
        data = pd.read_sql_query(query, conn)
        json_data = json.loads(data.to_json(orient="records", date_format="iso"))
        return jsonify(data=json_data)
    except Exception as e:
        print("################", str(e))
        return jsonify(error="Unable to fetch the data.")