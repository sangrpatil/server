import json
from flask import Blueprint, request, jsonify
from utility.platform_connector import PlatformConnector
import pandas as pd
from utility.batchfile import *


filters_blueprint = Blueprint('filter', __name__, url_prefix='/filter')

@filters_blueprint.route('/tables', methods=['GET'])
def get_tables(schema=None):
	try:
		conn = PlatformConnector().connect()
		schema = schema or request.args.get("schema")
		query = f"""SELECT table_schema, TABLE_NAME FROM
			(SELECT table_schema, TABLE_NAME FROM
			(select * from "{Database}"."{AUDIT_SCHEMA}"."JOB_MSTR" WHERE LOAD_TYPE = 'INCREMENT') J
			INNER JOIN
			(select table_schema, TABLE_NAME from information_Schema.tables
			where table_schema in ('STAGE', 'HYBRIS_BI')) T
			on T.TABLE_NAME= J.TGT_NM

			UNION
			SELECT table_schema, TABLE_NAME FROM
			(select * from "{Database}"."{AUDIT_SCHEMA}"."SRC_MSTR" WHERE LOAD_TYPE = 'INCREMENT') S
			INNER JOIN
			(select table_schema, TABLE_NAME from information_Schema.tables
			where table_schema in ('SOURCE_RAW')) TS
			on TS.TABLE_NAME= S.SRC_FILE_NAME
			)
			where table_schema='{schema}';
		"""
		data = pd.read_sql_query(query, conn)
		json_data = json.loads(data.to_json(orient="records", date_format="iso"))
		tables = [t.get("table_name") for t in json_data]
		return jsonify(data=tables)
	except Exception as e:
		print('#########', str(e))
		return jsonify(error="Unable to fetch the data.")

@filters_blueprint.route('/columns', methods=['GET'])
def get_columns(table=None):
	try:
		table_ = table or request.args.get("table")
		if not table_:
			return jsonify(error="Table not found")

		column_mapper = {
			"table": ["column 1", "column 2"]
		}
		columns = column_mapper.get(table_, [])
		return jsonify(data=columns)
	except Exception as e:
		return jsonify(error="Unable to fetch the data.")