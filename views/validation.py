import json

import pandas as pd
from flask import Blueprint, jsonify, request

from utility.platform_connector import PlatformConnector
from utility.definitions import *
from utility.batchfile import *

validation_blueprint = Blueprint('data-validation', __name__, url_prefix='/validate')


@validation_blueprint.route('/connect', methods=["GET", "POST"])
def connect():
	try:
		data = request.args or {}
		if data:
			conn_ = PlatformConnector().connect(**data)
			if isinstance(conn_, dict) and conn_.get('error'):
				return jsonify(status="Failure")
		else:
			PlatformConnector().connect()
		return jsonify(status="Success")
	except Exception as e:
		print("###########", str(e))
		return jsonify(status="Failure")


@validation_blueprint.route('/summary-report', methods=['GET'])
def summary_report():
	try:
		table = request.args.get("table")
		schema = request.args.get("schema")
		validation_rule = request.args.get("validation_rule")
		conn = PlatformConnector().connect()
		logs_data = get_validation_rule_mapper(schema, table, validation_rule)
		print("logs_data", logs_data)
		query = f"""select btch.btch_dt as Batch_date,validation_date,tablename,SOURCE_FILE_FLAG,ERR_DESC as error_desc,err_cnt as err_count,ERR_SVRTY as error_severity  
			from (
			(
			select btch_id,date as validation_date,err_mstr_id,err_cnt,tablename,'N' AS SOURCE_FILE_FLAG  FROM "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG"
			where cast(date as date) in (select max(cast(date as date)) from "{Database}"."{AUDIT_SCHEMA}"."ERR_LOG")
			union
			select btch_id,date as validation_date,err_mstr_id,err_cnt,filename as table_name,'Y' AS SOURCE_FILE_FLAG from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG"
			where cast(date as date) in (select max(cast(date as date)) from "{Database}"."{AUDIT_SCHEMA}"."FILE_ERR_LOG")
			)  all_err
			join "{Database}"."{AUDIT_SCHEMA}"."ERR_MSTR" mstr on ALL_ERR.ERR_MSTR_ID=mstr.ERR_CODE
			join "{Database}"."{AUDIT_SCHEMA}"."btch_stat" btch on all_err.btch_id =btch.btch_id

			) where tablename ='{table}' and err_desc='{validation_rule}'
			order by batch_date,validation_date desc
		;"""
		data = pd.read_sql_query(query, conn)
		json_data = json.loads(data.to_json(orient="records", date_format="iso"))
		[ d.update({
			"batch_date": d["batch_date"].split("T")[0]
			}) for d in json_data
		]
		return jsonify(data=json_data)
	except Exception as e:
		return jsonify(error="Unable to fetch the data. {}".format(str(e)))

@validation_blueprint.route('/logout', methods=['GET'])
def logout():
	try:
		PlatformConnector().close_connection()
		return jsonify(status="Success")
	except:
		return jsonify(error="Unable to close the connection")

def get_validation_rule_mapper(schema, table, validation_rule):
	try:
		# mapper = {schema: {validation_rule: function to execute}}
		print("##########", schema, validation_rule)
		mapper = {
			"SOURCE_RAW": {
				"Mandatory Field is missing": source_missingkey,
				"Date format check": s_dateformattest,
				"Duplicates in Primary Key/Composite Key": source_dupkey,
				"Special characters": src_specialcharactercheck,
				"True Duplicates": source_trueduplicate,
				"Negative value check": source_negativenumeric,
				"Numeric data check": source_numerictype
			},
			"STAGE-HYBRIS_BI": {
				"Mandatory Field is missing": tgt_missingkeytest,
				"Duplicates in Primary Key/Composite Key": tgt_dupkey,
				"Special characters": tgt_specialcharactercheck,
				"True Duplicates": tgt_trueduplicate,
				"SCD check": tgt_scdcheck
			}
		}
		special_char = '[#$^%"<>?/~`!]'
		schema_ = "SOURCE_RAW" if schema == "SOURCE_RAW" else "STAGE-HYBRIS_BI"
		print("Validation rule: ", mapper.get(schema_).get(validation_rule))
		if validation_rule == "Special characters":
			return mapper.get(schema_).get(validation_rule)(schema, table, special_char)
		else:
			return mapper.get(schema_).get(validation_rule)(schema, table)
	except Exception as e:
		print("error", str(e))
		return None